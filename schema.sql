/* creando el nodo persona */
CREATE CLASS Persona EXTENDS V;
CREATE PROPERTY Persona.numero INTEGER (MANDATORY TRUE);
CREATE INDEX Persona.numero UNIQUE;
CREATE PROPERTY Persona.profesion STRING;
CREATE PROPERTY Persona.cedula_pasaporte STRING;
CREATE PROPERTY Persona.sexo STRING;
CREATE PROPERTY Persona.nacionalidad STRING;
CREATE PROPERTY Persona.fecha_nacimiento DATE;
CREATE PROPERTY Persona.edad INTEGER;
CREATE PROPERTY Persona.condicion STRING;

/* configure the date */
ALTER DATABASE DATEFORMAT "dd/MM/yyyy";
ALTER DATABASE DATETIMEFORMAT "dd/MM/yyyy HH:mm:ss";

/* insertando algunas personas */
INSERT INTO Persona content {"numero": 595, 
                            "profesion": "obrero", 
                            "cedula_pasaporte": "047-0188136-1", 
                            "sexo": "masculino", 
                            "nacionalidad": "Dominicana", 
                            "fecha_nacimiento": "12/12/1990",
                            "edad": 29 }
                            
INSERT INTO Persona content {"numero": 596, 
                            "profesion": "profesor", 
                            "cedula_pasaporte": "035-2345621-0", 
                            "sexo": "masculino", 
                            "nacionalidad": "Mexicana", 
                            "fecha_nacimiento": "24/12/1984",
                            "edad": 35 }
                            
INSERT INTO Persona content {"numero": 597, 
                            "profesion": "psicologo", 
                            "cedula_pasaporte": "064-3356789-1", 
                            "sexo": "masculino", 
                            "nacionalidad": "Mexicana", 
                            "fecha_nacimiento": "15/04/1974",
                            "edad": 45 }
                            
INSERT INTO Persona content {"numero": 598, 
                            "profesion": "profesor", 
                            "cedula_pasaporte": "062-2467789-1", 
                            "sexo": "femenino", 
                            "nacionalidad": "Mexicana", 
                            "fecha_nacimiento": "15/04/1990",
                            "edad": 29,
                            "condicion": "fallecido"}
                            
INSERT INTO Persona content {"numero": 599, 
                            "profesion": "arquitecta", 
                            "cedula_pasaporte": "062-2467789-1", 
                            "sexo": "femenino", 
                            "nacionalidad": "Mexicana", 
                            "fecha_nacimiento": "21/03/1990",
                            "edad": 29 }
                            
INSERT INTO Persona content {"numero": 600, 
                            "profesion": "ingenieria en telecomunicaciones", 
                            "cedula_pasaporte": "062-2467789-1", 
                            "sexo": "femenino", 
                            "nacionalidad": "Mexicana", 
                            "fecha_nacimiento": "22/11/1966",
                            "edad": 53 }
                            
INSERT INTO Persona content {"numero": 601, 
                            "profesion": "bioquimico", 
                            "cedula_pasaporte": "062-2467789-1", 
                            "sexo": "femenino", 
                            "nacionalidad": "Mexicana", 
                            "fecha_nacimiento": "02/12/1996",
                            "edad": 23 }


CREATE CLASS Transporte EXTENDS V;
CREATE PROPERTY Transporte.marca STRING;
CREATE PROPERTY Transporte.tipo STRING;
CREATE PROPERTY Transporte.modelo STRING;
CREATE PROPERTY Transporte.placas STRING;
CREATE INDEX Transporte.placas UNIQUE;

/* insertando algunos transportes */
INSERT INTO Transporte content {"marca": "volkswagen", "tipo": "sedan", "modelo": "2004", "placas": "ZFK-759-B"};
INSERT INTO Transporte content {"marca": "honda", "tipo": "sedan", "modelo": "2012", "placas": "ZEW-178-A"};
INSERT INTO Transporte content {"marca": "nissan", "tipo": "sedan", "modelo": "2017", "placas": "YX-3845-A"};
INSERT INTO Transporte content {"marca": "tornado", "tipo": "pick-up", "modelo": "2013", "placas": "ZJ46261"};
INSERT INTO Transporte content {"marca": "ford", "tipo": "sedan", "modelo": "2009", "placas": "ZHJ-2824"};


CREATE CLASS Accidente EXTENDS V;
CREATE PROPERTY Accidente.OID INTEGER;
CREATE PROPERTY Accidente.fecha_hora DATETIME;
CREATE PROPERTY Accidente.tipo STRING;
CREATE PROPERTY Accidente.detalle STRING;
CREATE PROPERTY Accidente.factor_riesgo STRING;
CREATE PROPERTY Accidente.clima STRING;
CREATE PROPERTY Accidente.num_muertes INTEGER;

CREATE CLASS Ubicacion EXTENDS V;
CREATE PROPERTY Ubicacion.km INTEGER;
CREATE PROPERTY Ubicacion.tipo_via STRING;
CREATE PROPERTY Ubicacion.nombre_via STRING;
CREATE PROPERTY Ubicacion.tramo STRING;
CREATE PROPERTY Ubicacion.seccion_paraje STRING;
CREATE PROPERTY Ubicacion.referencia STRING;
CREATE PROPERTY Ubicacion.zona STRING;
CREATE PROPERTY Ubicacion.sector_barrio STRING;
CREATE PROPERTY Ubicacion.provincia STRING;
CREATE PROPERTY Ubicacion.municipio STRING;
CREATE PROPERTY Ubicacion.coordenadas EMBEDDED OPoint;
CREATE PROPERTY Ubicacion.superficie_rodamiento STRING;

CREATE PROPERTY Accidente.ubicacion EMBEDDED Ubicacion;


/* insertando algunos accidentes */
INSERT INTO Accidente content {"OID": 1 ,"fecha_hora": DATE("01/02/2018 07:00:00"), "tipo": "colisión con objeto fijo", "detalle": "Choque lateral", "factor_riesgo": "falla mecánica", "clima": "lluvioso", "num_muertes": 0, "ubicacion": {"km": 131, "tipo_via": "calle", "nombre_via": "obrero mundial", "tramo": "", "seccion_paraje": "", "referencia": "frente a finca urbana", "zona": "urbana", "sector_barrio": "", "provincia": "zacatecas", "municipio": "zacatecas", "coordinates": {"@class": "OPoint","coordinates" : [-15.966431, -48.101967]}, "superficie_rodamiento": "pavimentada"}}

INSERT INTO Accidente content {"OID": 2, "fecha_hora": DATE("03/02/2019 09:00:00"), "tipo": "colisión con vehículo automotor", "detalle": "Choque por alcance", "factor_riesgo": "conducción peligrosa", "clima": "soleado", "num_muertes": 1, "ubicacion": {"km": 145, "tipo_via": "avenida", "nombre_vía": "adolfo lopez mateos", "tramo": "", "seccion_paraje": "", "referencia": "frente al hotel arroyo de la plata", "zona": "urbana", "sector_barrio": "", "provincia": "zacatecas", "municipio": "zacatecas", "coordinates": {"@class": "OPoint","coordinates" : [-102.56680, 22.768316268]}, "superficie_rodamiento": "pavimentada"}}

INSERT INTO Accidente content {"OID": 3, "fecha_hora": DATE("06/05/2019 15:00:00"), "tipo": "colisión con peatón", "detalle": "Choque frontal", "factor_riesgo": "falla mecánica", "clima": "soleado", "num_muertes": 0, "ubicacion": {"km": 120, "tipo_via": "calle", "nombre_vía": "guerrero", "tramo": "", "seccion_paraje": "", "referencia": "col. centro", "zona": "urbana", "sector_barrio": "", "provincia": "zacatecas", "municipio": "zacatecas", "coordinates": {"@class": "OPoint","coordinates" : [-91.2346400, 17.9303900]}, "superficie_rodamiento": "pavimentada"}}

INSERT INTO Accidente content {"OID": 4, "fecha_hora": DATE("10/04/2018 07:00:00"), "tipo": "volcadura", "detalle": "volcadura de vehículo", "factor_riesgo": "falla mecánica", "clima": "templado", "num_muertes": 0, "ubicacion": {"km": 100, "tipo_via": "calle", "nombre_vía": "garcía salinas", "tramo": "", "seccion_paraje": "", "referencia": "frente a tienda oxxo", "zona": "urbana", "sector_barrio": "", "provincia": "zacatecas", "municipio": "guadalupe", "coordinates": {"@class": "OPoint","coordinates" : [-102.9876221, 22.6468746]}, "superficie_rodamiento": "pavimentada"}}



/* creando la relación conductor */
CREATE CLASS Conductor EXTENDS E;
CREATE PROPERTY Conductor.aliento_alcoholico STRING;
CREATE PROPERTY Conductor.cinturon_seguridad STRING;

/* creando la relación pasajero */
CREATE CLASS Pasajero EXTENDS E;
CREATE PROPERTY Pasajero.aliento_alcoholico STRING;
CREATE PROPERTY Pasajero.cinturon_seguridad STRING;

/* creando la relación involucrado */
CREATE CLASS Involucrado EXTENDS E;
CREATE PROPERTY Involucrado.causa STRING;


/* creando relaciones de prueba con un accidente */
CREATE EDGE Conductor FROM (SELECT FROM Persona WHERE numero = 595) TO 
                            (SELECT FROM Transporte WHERE placas = "ZEW-178-A") SET aliento_alcoholico = 'sí', cinturon_seguridad = 'no';
                            
CREATE EDGE Involucrado FROM (SELECT FROM Transporte WHERE placas = "ZEW-178-A") TO
                             (SELECT FROM Accidente WHERE OID = 2 ) SET causa = 'Uso de alcohol';
                             
CREATE EDGE Pasajero FROM (SELECT FROM Persona WHERE numero = 634) TO 
                            (SELECT FROM Transporte WHERE placas = "ZEW-178-A") SET aliento_alcoholico = 'no', cinturon_seguridad = 'no';

CREATE EDGE Involucrado FROM (SELECT FROM Accidente WHERE OID = 2 ) TO
                             (SELECT FROM Transporte WHERE placas = "ZHJ-2824");

CREATE EDGE Conductor FROM (SELECT FROM Persona WHERE numero = 612) TO 
                            (SELECT FROM Transporte WHERE placas = "ZHJ-2824") SET aliento_alcoholico = 'no', cinturon_seguridad = 'no';

CREATE EDGE Involucrado FROM (SELECT FROM Accidente WHERE OID = 2) TO
                             (SELECT FROM Persona WHERE numero = 599 ) SET causa = 'Peatón cercano';
                             
                             
                             
/* Accidente 2*/
CREATE EDGE Conductor FROM (SELECT FROM Persona WHERE numero = 600) TO 
                            (SELECT FROM Transporte WHERE placas = "ZFK-759-B") SET aliento_alcoholico = 'no', cinturon_seguridad = 'sí';
                            
CREATE EDGE Involucrado FROM (SELECT FROM Transporte WHERE placas = "ZFK-759-B") TO
                             (SELECT FROM Accidente WHERE OID = 3 ) SET causa = 'Falla mécanica';
                             
CREATE EDGE Conductor FROM (SELECT FROM Persona WHERE numero = 601) TO 
                            (SELECT FROM Transporte WHERE placas = "ZHJ-2824") SET aliento_alcoholico = 'no', cinturon_seguridad = 'no';
                             
CREATE EDGE Involucrado FROM (SELECT FROM Accidente WHERE OID = 3 ) TO
                             (SELECT FROM Transporte WHERE placas = "ZHJ-2824"); 
                             
                             
/* Accidente 3 */
CREATE EDGE Conductor FROM (SELECT FROM Persona WHERE numero = 598) TO 
                            (SELECT FROM Transporte WHERE placas = "YX-3845-A") SET aliento_alcoholico = 'no', cinturon_seguridad = 'sí';
                            
CREATE EDGE Involucrado FROM (SELECT FROM Transporte WHERE placas = "YX-3845-A") TO
                             (SELECT FROM Accidente WHERE OID = 4 ) SET causa = 'Falla mécanica';
                             
CREATE EDGE Conductor FROM (SELECT FROM Persona WHERE numero = 597) TO 
                            (SELECT FROM Transporte WHERE placas = "ZJ46261") SET aliento_alcoholico = 'no', cinturon_seguridad = 'no';
                             
CREATE EDGE Involucrado FROM (SELECT FROM Accidente WHERE OID = 4 ) TO
                             (SELECT FROM Transporte WHERE placas = "ZJ46261"); 
                             
                             
/* some queries */

/* 1. ¿Cuáles son los vehículos involucrados y la causa del accidente? */
SELECT tc.outV().placas as placas, tc.causa as causa, tc.outV().placas as involucrado  
FROM (SELECT inE('involucrado') as tc, outE('involucrado') as tv 
FROM Accidente WHERE OID = 2) unwind placas, causa, involucrado 

/* 2. ¿Hubo peatones involucrados en el accidente? */
SELECT OUT('involucrado').numero as numero FROM Accidente WHERE OID = 2 UNWIND numero

/* 3.  ¿Cuáles han sido las principales causas de accidentes? */
SELECT inE('involucrado').causa as causa FROM Accidente unwind causa

/* 4. ¿Han ocurrido accidentes a muy cercana distancia? */
SELECT 

/* 5. ¿Cuántos fallecidos ha habido por accidente? */
select OID, num_muertes FROM Accidente

/* 6. ¿Cuántos accidentes han ocurrido por mes? */
SELECT fecha_hora.format('MM') AS mes, COUNT(*) AS total FROM Accidente ORDER BY fecha_hora

/* 7. 